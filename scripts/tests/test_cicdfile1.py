import unittest
from scripts.cicd_file1 import pname


class Testcicd(unittest.TestCase):
    def testpnamefuncation(self):
        out = pname("Python Test")
        self.assertEqual(out, "Hello your name is Python Test")


if __name__ == "__main__":
    unittest.main()
