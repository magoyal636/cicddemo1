"""Print your name using the argparse"""
import argparse


def pname(name):
    return f"Hello your name is {name}"


if __name__ == '__main__':
    parser = argparse.ArgumentParser(__doc__)
    parser.add_argument("--name", help="enter your name", required=True)
    args = parser.parse_args()
    out = pname(args.name)
    print(out)
