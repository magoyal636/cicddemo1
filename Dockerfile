FROM fedora:36

RUN dnf install pip ansible git yamllint -y

RUN pip3 install tox pytest

RUN dnf clean all
